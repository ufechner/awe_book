# -*- coding: utf-8 -*-
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University 
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""    
"""
Module to create diagrams of the the total efficiency as a function of the pumping efficiency
for different groundstations.
"""
import numpy as np

from BaseClasses import createDir, setFigLinesBW
from os.path import expanduser
import matplotlib as mpl
from Settings import LINEWIDTH, USEPGF, BOOK


if USEPGF:
    mpl.use('pgf')
    pgf_with_rc_fonts = {
        "font.family": "serif",
        "font.serif": [],                   # use latex default serif font
    }
    mpl.rcParams.update({'font.size': 9})
    mpl.rcParams.update(pgf_with_rc_fonts)
else:
    if BOOK:
        mpl.rcParams.update({"font.family": "serif"})
        mpl.rcParams.update({"text.usetex" : "True"})
        mpl.rcParams.update({'font.size': 12})
        mpl.rc('xtick', labelsize=13) 
        mpl.rc('ytick', labelsize=13)  
    
import matplotlib.pyplot as plt

class Efficiency(object):

    def __init__(self, eta_batt = 0.95):
        assert eta_batt > 0
        assert eta_batt <= 1.0
        self.eta_batt = eta_batt

    def calcEtaElectricalGross(self, eta_pumping, eta_eo, eta_ei):
        """ 
        Calculate the electrical gross efficiency as a function of the mechanical pumping efficiency, the
        electrical reel-out efficiency and the electrical reel-in efficiency.
        """
        assert eta_pumping > 0
        assert eta_eo > 0
        assert eta_ei > 0
        assert eta_pumping <= 1.0
        assert eta_eo <= 1.0
        assert eta_ei <= 1.0     
        result = (eta_eo * eta_ei * self.eta_batt - 1.0 + eta_pumping) / (eta_ei * self.eta_batt * eta_pumping)
        return result
        
    def plotEtaElectricalGross(self, eta_eo, eta_ei, eta_eo2, eta_ei2):
        list_eta_pumping, list_eta, list_eta2 = [], [], []                
        for eta_pumping in np.linspace(0.6, 1.0, 20):
            eta = self.calcEtaElectricalGross(eta_pumping, eta_eo, eta_ei)
            eta2 = self.calcEtaElectricalGross(eta_pumping, eta_eo2, eta_ei2)
            list_eta_pumping.append(eta_pumping * 100.0)
            list_eta.append(eta * 100.0)
            list_eta2.append(eta2 * 100.0) 
        if USEPGF:              
            size = 0.65
            self.fig1 = plt.figure(1, figsize=(4.8 * 0.7, 3.5 * size))
            setFigLinesBW(self.fig1)
        else:
            size = 1.3
            self.fig1 = plt.figure(5, figsize=(4.8*size, 3.5*size))
        plt.ax1 = plt.subplot(111)  
        #plt.xlim(10.0, 90.0)
        plt.ax1.set_ylabel(u'Gross electrical efficiency [\%]')                  
        line1 = plt.plot(list_eta_pumping, list_eta2, linewidth = LINEWIDTH, color="blue", label="53.5 kW Groundstation$^4$") 
        line2 = plt.plot(list_eta_pumping, list_eta, linewidth = LINEWIDTH, color="red",   label="20.0 kW Demonstrator$^4$") 
        plt.ax1.set_xlabel(u'Pumping efficiency [\%]')      
        lines = line1 + line2
        setFigLinesBW(self.fig1)
        if USEPGF:
            leg = plt.ax1.legend(lines, [l.get_label() for l in lines], bbox_to_anchor=(1.075, 0.03), loc='lower right', frameon=False, prop={'size':9})
        else:
            leg = plt.ax1.legend(lines, [l.get_label() for l in lines], bbox_to_anchor=(0.97, 0.025), loc='lower right', frameon=False, prop={'size':13})
        leg.draw_frame(True)
        plt.ax1.grid(True, color='0.5') # grey
        self.fig1.tight_layout()   
        plt.ylim(40.0, 95.0)
        #plt.subplots_adjust(left=0.35, right = 1.1)
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/gross_electrical_efficiencies')        
        plt.savefig('./results/gross_electrical_efficiencies.png')
        if USEPGF:                 
            plt.savefig(base_file_name + '.pgf')   
            print "Written file: "+ base_file_name + '.pgf'        
        plt.show()
        print "Finshed!"
        return self.fig1            
        

if __name__=="__main__":
    eff = Efficiency()
    print eff.plotEtaElectricalGross(0.8, 0.79, 0.905, 0.9)
