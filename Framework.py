# -*- coding: utf-8 -*-
# pylint: disable-msg = W0621
from __future__ import division
from __future__ import print_function
"""
Framework for calculating the speed, power and force of a kite power system
based on an averaged system model.
"""
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
from pylab import np
from FuncDesigner import oovar, cos, sin, sqrt
from openopt import SNLE, NLP
from math import pi
from BaseClasses import Serialize
from scipy.optimize import minimize_scalar, minimize

class SimpleKite(Serialize):
    """ Class for storing the major properties of a kite.     """
    def __init__(self, mass, area, paramCL, paramLoD, reelInLoD, reelInCD, name = ""):
        """
        **Parameters**

        :param mass:      mass of the kite, the control pod and a part of the tether
        :param area:      effective area of the kite [m²]
        :param paramCL:   lift coeficient of the kite
        :param paramLoD:  lift over drag ration of the kite
        """
        self.mass      = mass
        self.area      = area
        self.paramCL   = paramCL
        self.paramLoD  = paramLoD
        self.reelInLoD = reelInLoD
        self.reelInCD  = reelInCD
        self.name = name

class Generator(Serialize):
    """  Class for simulating the efficiency of the generator.
         Default values based on the datasheet of 500STK8M from Alxion.

         **Parameters**

         :ampere_per_nm:   generator constant c_g [A/(Nm)]
         :resistance:      resistance of one winding [Ohm]
         :ratio_k:         ratio of total electric losses and DC copper losses
         :tau_c:           coulomb friction [Nm]
    """
    def __init__(self, ampere_per_nm = 0.08563, resistance = 0.04, ratio_k = 1.2,
                 tau_c = 66.8, c_viscous_friction = 0.0):
        self.ampere_per_nm = ampere_per_nm
        self.resistance = resistance
        self.ratio_k = ratio_k
        self.tau_c = tau_c
        self.c_viscous_friction = c_viscous_friction
    def calcEfficiency(self, torque = 478, rpm = 538):
        """
        **Parameters**

        :param torque:   torque at the input of the generator [Nm]
        :param rpm:      speed of the generator axis in rotations per minute
        """
        assert torque > 0
        assert rpm > 0
        current = self.ampere_per_nm * torque
        print ("current: ", current)
        omega = pi / 30.0 * rpm
        p_loss = current**2 * self.resistance * 3.0 * self.ratio_k + self.tau_c * omega
        p_mech = torque * rpm / 60.0 * 2.0 * pi
        p_elec = p_mech - p_loss
        eff = p_elec / p_mech
        return eff

class Motor(Serialize):
    """  Class for simulating the efficiency of the motor.
         Default values based on the datasheet of 500STK8M from Alxion.
         **Parameters**

         :ampere_per_nm:   generator constant c_g [A/(Nm)]
         :resistance:      resistance of one winding [Ohm]
         :ratio_k:         ratio of total electric losses and DC copper losses
         :tau_c:           coulomb friction [Nm]
    """
    def __init__(self, ampere_per_nm = 0.08563, resistance = 0.04, ratio_k = 1.2,
                 tau_c = 66.8, c_viscous_friction = 0.0):
        self.ampere_per_nm = ampere_per_nm
        self.resistance = resistance
        self.ratio_k = ratio_k
        self.tau_c = tau_c
        self.c_viscous_friction = c_viscous_friction
    def calcEfficiency(self, torque = 478, rpm = 538):
        """
        **Parameters**

        :param torque:   torque at the input of the generator [Nm]
        :param rpm:      speed of the generator axis in rotations per minute
        """
        assert torque > 0
        assert rpm > 0
        current = self.ampere_per_nm * torque
        omega = pi / 30.0 * rpm
        p_loss = current**2 * self.resistance * 3.0 * self.ratio_k + self.tau_c * omega
        p_mech = torque * rpm /60.0 * 2.0 * pi
        p_elec = p_mech + p_loss
        eff = p_mech / p_elec
        return eff

class SimpleWinch(Serialize):
    """ Class for storing the major properties of a groundstation (winch).     """
    def __init__(self, v_reel_out_max=8.0, v_reel_in_max=8.0, force_max=400, p_el_nom=18500,
                 diameter = 0.3, eta_generator=0.905, generator=None, motor=None, name=""):
        self.v_reel_out_max = v_reel_out_max
        self.v_reel_in_max  = v_reel_in_max
        self.force_max = force_max
        self.p_el_nom = p_el_nom
        self.eta_controller = 0.98 # efficiency of the motor and generator controller(s)
        self.eta_batt = 0.95       # efficiency of the energy storage system, e.g. battery
        self.crest_factor_speed = 1.2  # max reel-out speed / average reel-out speed
        self.crest_factor_force = 1.11 # max reel-out force / average reel-out force
        self.eta_generator = eta_generator
        self.generator = generator
        self.motor = motor
        self.diameter = diameter
        self.name = name
        self.stored_energy = 0 # energy, stored in the battery (or produced)

    def calcEtaSystem(self, v_tether, gross_electrical_energy, cycle_time):
        """ Calculate the system efficiency due to the power needed for the spindle motor,
            the brakes and the control system. """
        assert v_tether >= 0
        assert cycle_time > 0
        assert gross_electrical_energy > 0
        # measured power for two brakes and PLC
        brake_power = 170
        # 600 W at 6 m/s
        spindle_power = (600.0 * v_tether / 6.0)
        losses = (spindle_power + brake_power) * cycle_time
        self.stored_energy -= losses
        return (gross_electrical_energy - losses) / gross_electrical_energy

    def calcEtaReelOut(self, reel_out_energy, reel_out_time, v_tether):
        assert reel_out_energy > 0
        assert reel_out_time > 0
        assert v_tether > 0
        p_mech_o = reel_out_energy / reel_out_time
        f_o = p_mech_o / v_tether
        print ("p_mech_o, f_o: ", p_mech_o, f_o)
        torque_o = f_o * (self.diameter / 2.0)
        rpm_o = (v_tether / (self.diameter * pi)) * 60.0
        print ("torque_o, rpm_o: ", torque_o, rpm_o)
        # motor and generator in parallel during reel-in, 80% of the torque on the generator, 20% motor
        eta_og = self.generator.calcEfficiency(torque_o * 0.8, rpm_o) * self.eta_controller
        eta_om = self.motor.calcEfficiency(torque_o * 0.2, rpm_o) * self.eta_controller
        eta_o = 0.8 * eta_og + 0.2 * eta_om
        self.stored_energy += eta_o * reel_out_energy
        print ("self.stored_energy, eta_og, eta_om, eta_o: ", self.stored_energy, eta_og, eta_om, eta_o)
        return eta_o

    def calcEtaReelIn(self, reel_in_energy, reel_in_time, v_tether):
        assert reel_in_energy > 0
        assert reel_in_time > 0
        assert v_tether > 0
        p_mech_i = reel_in_energy / reel_in_time
        f_i = p_mech_i / v_tether
        torque_i = f_i * (self.diameter / 2.0)
        rpm_i = (v_tether / (self.diameter * pi)) * 60.0
        eta_i = self.motor.calcEfficiency(torque_i, rpm_i) * self.eta_controller
        self.stored_energy -= ((reel_in_energy / eta_i) / self.eta_batt)
        print ("self.stored_energy: ", self.stored_energy)
        return eta_i

    def getMaxAverageMechInputPower(self):
        """ Calculate the maximal average mechanical reel-out power that the groundstation
            can handle. Assumptions:
                1. The max. mech input is higher than the nom. electrical power because of the mechanical losses
                   in the drive train (gearbox, pulleys).
                2. Because the power during reel-in is much lower than during reel-out the generator can cool
                   down during reel-in. Therefore we assume that running the generator at 115% of the nominell
                   power during reel-out is acceptable.
        """
        return (self.p_el_nom / self.eta_generator * 1.15)

class Framework(Serialize):
    """ Framework for calculating averaged kite and tether properties like the tether force """
    def __init__(self, phi, delta, kite, winch):
        """
        **Parameters**

        :param phi:   azimuth angle in radian
        :param delta: 0 = flying horizontally, pi/2 = flying upwards
        :param kite:  instance of SimpleKite
        """
        self.phi      = phi
        self.delta    = delta
        self.g_earth  = 9.81          # gravitiy of the earth [m/s²]
        self.rho      = 1.25          # air density [kg/m³]
        self.paramCL  = kite.paramCL
        self.paramLoD = kite.paramLoD
        self.area     = kite.area
        self.beta     = 0             # elevation angle
        self.tetherLength = 0
        self.reel_out_time = 0
        self.reel_in_time = 40.0
        self.reel_in_power = 0.0
        self.reelInEnergy = 1.0e5 # Joule
        self.delta_length = 200.0
        self.reel_out_power = 0
        self.reel_in_LoD = kite.reelInLoD
        self.reel_in_CD  = kite.reelInCD
        self.winch = winch
        self.kite = kite
        self.transition_time = 5.0 # this is the 50% of the time to change from full depower to full power [s]
        self.beta_list = []   # list of elevation angles for plotting
        self.length_list = [] # list of tether length values for plotting
        self.av_power_list = [] # list of average power estimates during reel-in

    def calcWindHeight(self, v_wind_gnd, height, ref_height = 10.0):
        alpha = 1 / 7.0
        return (v_wind_gnd * (height / ref_height)**alpha)

    def calcWind(self, l_av, v_wind_gnd, beta):
        h_av = l_av * sin(beta)
        return self.calcWindHeight(v_wind_gnd, h_av)

    def calcF1(self, v_wind, mass, b_over_c, beta, rel_speed, paramLoD):
        """
        Calculate tether force one; must be in equilibrium with tether force two.

        **Parameters**

        :param v_wind:    wind speed at the height of the kite relative to ground
        :param mass:      mass of the kite, the control pod and a part of the tether
        :param b_over_c:  b/c (see fig. 3.1 in main.pdf)
        :param beta:      elevation angle in radian
        :param rel_speed: reel-out speed devided by wind speed
        :param paramLoD:  effective lift over drag ratio
        """
        return (0.25 * self.paramCL**2 * self.rho**2 * self.area**2 * (1+1 / paramLoD**2)  \
                   * (cos(beta) * cos(self.phi) - rel_speed)**4 * v_wind**4 * (1 + b_over_c**2)**2 \
                   - (mass * self.g_earth)**2 * cos(beta)**2)**0.5 - (mass * self.g_earth) * sin(beta)

    def calcF2(self, v_wind, mass, b_over_c, beta, rel_speed, paramLoD):
        """
        Calculate tether force two; must be in equilibrium with tether force one.

        **Parameters**

        :param v_wind:    wind speed at the height of the kite relative to ground
        :param mass:      mass of the kite, the control pod and a part of the tether
        :param b_over_c:  b/c (see fig. 3.1 in main.pdf)
        :param beta:      elevation angle in radian
        :param rel_speed: reel-out speed devided by wind speed
        :param paramLoD:  effective lift over drag ratio
        """
        dragForce = 0.5 * self.paramCL* self.rho * self.area * (cos(beta) * cos(self.phi) - rel_speed)**2 \
            * v_wind**2 * (1 + b_over_c**2) / paramLoD
        # relTangentialVelocity: tangential velocity / abs(wind velocity) at height of the kite
        relTangentialVelocity = ((cos(beta) * cos(self.phi) - rel_speed)**2 * paramLoD**2 \
            + cos(beta)**2 * cos(self.phi)**2 - 1)**0.5
        # weightForce: projection of the weight force vector in the direction of v apparent
        weightForce = mass * self.g_earth * (-(cos(beta) * cos(self.phi) - rel_speed) * sin(beta) + cos(beta) \
              * sin(self.phi) + cos(beta) * sin(self.delta) * relTangentialVelocity) \
              / ((cos(beta) * cos(self.phi) - rel_speed) * (1 + b_over_c**2)**0.5)
        return (1 + b_over_c**2)**0.5 * (dragForce - weightForce)

    def calcFB(self, v_wind, mass, beta, rel_speed, paramLoD):
        """
        Calculate the tether force and b_over_c by solving the nonlinear equation system calcF1
        and calcF2 for force and b_over_c.

        **Parameters**

        :param v_wind:    wind speed at the height of the kite relative to ground
        :param mass:      mass of the kite, the control pod and a part of the tether
        :param beta:      elevation angle in radian
        :param rel_speed: reel-out speed devided by wind speed
        :param paramLoD:  effective lift over drag ratio
        :returns:         tuple of (force, b_over_c)
        """
        b_over_c = oovar()     # b over c    (see fig 3.1 in main.pdf)
        force    = oovar()     # tetherforce (T in main.pdf)

        startPoint = {b_over_c:paramLoD, force:self.calcF1(v_wind, paramLoD, beta, rel_speed, paramLoD, mass)}
        # the equations have to be defined as expressions that are equal to zero
        eq1 = (self.calcF1(v_wind, mass, b_over_c, beta, rel_speed, paramLoD)) - force
        eq2 = (self.calcF2(v_wind, mass, b_over_c, beta, rel_speed, paramLoD)) - force
        equations = (eq1, eq2)
        problem = SNLE(equations, startPoint, istop=100, ftol = 1e-4) #SNLE: System of Non-Linear Equations

        problem.constraints = [b_over_c <= paramLoD, b_over_c > paramLoD/1.5, force > 0, \
                               force <= self.calcF1(v_wind, paramLoD, beta, rel_speed, paramLoD, mass)]
        res = problem.solve('interalg')
        if res.stopcase == 1:
            b_over_c_found = res(b_over_c)
            force_found = res(force)
        else:
            b_over_c_found = np.nan
            force_found = np.nan
        return (force_found, b_over_c_found, )

    def calcTangentialKiteVelocityFactor(self, beta, rel_speed, delta=np.pi/2.0, phi = 0):
        """ Reelin phase: Kite flies upwards -> delta = 90 degrees
            Azimuth angle: phi, elevation angle: beta
        """
        result = -cos(phi) * sin(beta) * sin(delta) - sin(phi) * cos(delta) + sqrt(cos(phi) ** 2 * sin(beta) ** 2
                 * sin(delta) ** 2 + 0.2e1 * cos(phi) * sin(beta) * sin(delta) * sin(phi) * cos(delta)
                 + sin(phi) ** 2 * cos(delta) ** 2 - 0.1e1 + cos(phi) ** 2 * cos(beta) ** 2
                 + self.reel_in_LoD ** 2 *(cos(phi) * cos(beta)  -  rel_speed) ** 2)
        return result

    def calcBetaDelta(self, tangKiteVelFac, v_wind, delta_t):
        """ v_wind: absolute wind velocity at the height of the kite (skalar)
        returns the change of beta; beta is the elevation angle in radian
        """
        return (1/self.tetherLength * tangKiteVelFac * v_wind * delta_t)

    def calcTetherLengthDelta(self, rel_speed, delta_t, v_wind):
        """ v_wind: absolute wind velocity at the height of the kite (skalar) """
        return rel_speed * v_wind * delta_t

    def reelIn(self, max_tether_length, beta, reel_out_time, reel_out_power, delta_length, v_wind, log = True):
        """
        max_tether_length: maximal tether length, length when reel-in phase is started
        delta_length:  difference between max. and min. tether length """
        self.reel_in_time = 0
        self.reel_in_power = 0
        self.tetherLength = max_tether_length
        self.beta = beta
        self.reel_out_time = reel_out_time
        self.reel_out_power = reel_out_power
        self.delta_length = delta_length
        ###
        # start iteration with an estimate for the optimal reel-in speed
        rel_speed_est = -1.0
        if abs(rel_speed_est * v_wind) > self.winch.v_reel_in_max:
            rel_speed_est = -self.winch.v_reel_in_max / v_wind

        # reel out in steps of one second
        if log:
            print("reel_in_time", "tetherLength", "beta [deg]", "v_reelout [m/s]", "netMechPower", "reelInForce", sep='\t')
        while self.tetherLength > (max_tether_length - delta_length):

            v_wind_kite_altitude = self.calcWind(self.tetherLength, v_wind, self.beta)
            rel_speed_est = self.reelInStep(0.1, rel_speed_est, v_wind_kite_altitude)
            #print "=====>> ", round(self.reel_in_time,2), self.beta*180/np.pi, round(rel_speed_est,2), \
            #                  round((rel_speed_est * v_wind_kite_altitude),2)
            netMechPower, reelInEnergy, reelOutEnergy, reelInForce = self.calcAveragePowerFullCycle(rel_speed_est,
                                                                                       v_wind_kite_altitude)
            self.beta_list.append(self.beta)
            self.length_list.append(self.tetherLength)
            if log:
                beta_deg=self.beta * 180.0 / np.pi
                reelout_speed = rel_speed_est * v_wind_kite_altitude
                print ("%.2f" %self.reel_in_time, "%.2f" %self.tetherLength, "%.2f" %beta_deg, \
                                         "%.2f" %reelout_speed, "%.2f" %netMechPower, "%.2f" %reelInForce, sep='\t\t')
            assert self.reel_in_time < 200.0
        self.reelInEnergy = reelInEnergy
        return (netMechPower, reelInEnergy, reelOutEnergy)

    def calcReelInForce(self, rel_speed, v_wind = None):
        """
        Calculate the reel-in force (no crosswind).

        **Parameters**

        :param v_wind:    wind speed at the height of the kite relative to ground
        :param rel_speed: reel-out speed devided by wind speed
        """
        if v_wind is None:
            v_wind = self.v_wind_kite_altitude
        v_app = (cos(self.beta) * cos(self.phi) - rel_speed) * v_wind * (1 + self.reel_in_LoD**2)**0.5
        return (self.reel_in_CD * (1 + self.reel_in_LoD**2)**0.5 * 0.5 * self.rho * v_app**2 * self.area)

    def calcAveragePowerFullCycle(self, rel_speed, v_wind):
        """
        rel_speed:   relative reel-in speed; during reel-in < 0
        reelInForce: force for the given rel_speed
        """
        assert(rel_speed != 0)
        assert(v_wind != 0)
        # print "====>>>> ", rel_speed, v_wind, -round(rel_speed * v_wind, 2)
        reelInForce = self.calcReelInForce(rel_speed, v_wind)
        reelOutEnergy = self.reel_out_time * self.reel_out_power
        reel_in_time = (self.delta_length / (-rel_speed * v_wind)) + self.transition_time
        reelInEnergy = rel_speed * v_wind * reelInForce * (reel_in_time - self.transition_time)
        # print "==========>>>> ", round(reel_in_time,3), self.beta*180.0/np.pi, round(reelInEnergy/1000.0)
        # reelInLosses = rel_speed * v_wind * 375.0 * reel_in_time # friction of the winch
        # print "------------>", reelInForce, reelOutEnergy, reel_in_time, reelInEnergy
        netMechPower = (reelOutEnergy + reelInEnergy) / (self.reel_out_time + reel_in_time)
        #cycleEfficiency = (reelOutEnergy + reelInEnergy) / reelOutEnergy
        #print "CylceEfficiency: " + str(cycleEfficiency)
        return (netMechPower, reelInEnergy, reelOutEnergy, reelInForce)

    def calcAvPower(self, speed):
        # print('speed: ', speed)
        av_power, dummy, dummy, dummy = self.calcAveragePowerFullCycle(speed, self.v_wind_kite_altitude)
        return -av_power

    def reelInStep(self, delta_t, rel_speed, v_wind_kite_altitude):
        """ rel_speed: relative reel-in speed; during reel-in < 0 """
        deltaLength = self.calcTetherLengthDelta(rel_speed, delta_t, v_wind_kite_altitude)
        self.tetherLength += deltaLength
        tangKiteVelFac = self.calcTangentialKiteVelocityFactor(self.beta, rel_speed)
        self.beta += self.calcBetaDelta(tangKiteVelFac, v_wind_kite_altitude, delta_t)
        av_power0, dummy, dummy, dummy = self.calcAveragePowerFullCycle(rel_speed, v_wind_kite_altitude)
        step_factor = 1.02
        speed1 = rel_speed * step_factor
        # limit the reel out speed to the max. speed of the winch
        if abs(speed1 * v_wind_kite_altitude) > self.winch.v_reel_in_max:
            speed1 = - self.winch.v_reel_in_max / v_wind_kite_altitude

        self.v_wind_kite_altitude = v_wind_kite_altitude
        # rel_speed = min(rel_speed, -0.05)
        min_speed = rel_speed / 4.0 # speeds are always negative (reel-in)
        max_speed = speed1
        # print(min_speed, max_speed, self.calcAvPower(min_speed), self.calcAvPower(max_speed))
        if False:
            res = minimize_scalar(self.calcAvPower, bounds=(max_speed, min_speed), method='bounded', tol = 1e-2)
            rel_speed = res.x
            assert res.success
        elif False:
            x0 = [(min_speed + max_speed) / 2.0]
            # bnds = ((max_speed, min_speed),)
            # the force must not exceed the max tether force
            cons = ({'type': 'ineq', 'fun': lambda x:  self.winch.force_max - self.calcReelInForce(x)})
            res = minimize(self.calcAvPower, x0, method='SLSQP', tol = 1e-2, options={'disp': False}, constraints=cons)
            rel_speed = res.x[0]
            # print('Force: ', self.calcReelInForce(rel_speed))
            if not res.success:
                print('Force: ', self.calcReelInForce(rel_speed))
                print(res)
            assert res.success
        else:
            x0 = [(min_speed + max_speed) / 2.0]
            lb = [max_speed] # lower bound
            ub = [min_speed] # upper bound
            # print('max_speed, min_speed', max_speed, min_speed)
            # the force must not exceed the max tether force
            cons = lambda x:  -self.winch.force_max + self.calcReelInForce(x)
            # cons = lambda x:  -4000.0 + self.calcReelInForce(x)
            prob = NLP(self.calcAvPower, x0, c=cons, lb=lb, ub=ub, tol = 1e-2)
            prob.maxIter = 10
            res = prob.solve('ralg', iprint = -1)
            rel_speed = res.xf[0]
            assert res.isFeasible
            # print('Force: ', self.calcReelInForce(rel_speed))

        power_opt = -self.calcAvPower(rel_speed)
        self.av_power_list.append(power_opt)
        #print ("rel_speed_opt, power_opt: ", rel_speed, power_opt)
        #sys.exit(0)
        self.reel_in_time += delta_t
        return rel_speed

if __name__ == '__main__':
    from Repository import Repository
    repo = Repository()
    kite = repo.getKite('Kite_295_LoD_7_1')
    winch = repo.getWinch('Winch_20000N_4.4_8.8')
    ## define operational parameters
    beta  = 20.0 * (np.pi/180)
    phi   = 0 * (np.pi/180)
    delta = 0 * (np.pi/180)
    ## create framework object
    framework = Framework(phi, delta, kite, winch)
    max_tether_length = 500.0
    reel_out_time = 60.0
    reel_out_power = 3000.0
    delta_length = 200.0
    v_wind = 4.0
    if True:
        framework.reelIn(max_tether_length, beta, reel_out_time, reel_out_power, delta_length, v_wind, False)
        # print (framework.beta_list)
        # print (framework.length_list)
        # print (framework.av_power_list)
        if True:
            from pylab import np, plot, xlim, ylim, legend, grid, gca
            n = len(framework.beta_list)
            H, D = [], [] # list of heights
            for i in range(n):
                len1 = framework.length_list[i]
                H.append(sin(framework.beta_list[i]) * len1)
                D.append(cos(framework.beta_list[i]) * len1)
            plot(D, H)
            ylim(0.0, 300.0)
            xlim(0.0, 500.0)
            legend(loc='upper right')
            gca().set_ylabel(u'Height [m]')
            gca().set_xlabel(u'Kite distance [m]')
            grid(True, color='0.25')
        if True:
            from pylab import np, plot, xlim, ylim, legend, grid, gca
            n = len(framework.beta_list)

    if False:
        reelOutEnergy = 2279956.76841
        reel_out_time = 34.1993885011
        reel_out_speed = 5.84805778015
        eta_ro = framework.winch.calcEtaReelOut(reelOutEnergy, reel_out_time, reel_out_speed)
        print ("eta_ro: ", eta_ro)
