# -*- coding: utf-8 -*-
""" Global settings for the kite-power system optimizer. """
SETTINGS = 3
if SETTINGS == 1:
    # basic test case; time used by interalg: 46 s on 4 GHz core i7
    LINEWIDTH = 1.0
    LEGEND_FRAME = True
    USEPGF = False   # set it to true if you want to export diagrams for latex import as .pgf files
    BOOK = False     # set it to true to use the latex backend to produce pdf files
    MAX_ITER = 200                   # maximal number of iterations for solver de
    MAX_ITER_INTERALG = MAX_ITER / 2 # maximal number of iterations for solver interalg
    V_WIND_MIN =   9.0 # minimal wind speed
    V_WIND_MAX =  10.0 # maximal wind speed
    USE_SOLVER_DE = False # the solver de is faster, but less accurate
    OUTER_LOOP = 2 # number of the iterations in the outer loop; should be between 2 (fast) and 8 (accurate)
if SETTINGS == 2:
    # second test case; v_wind = 9.0 .. 15.00; 4 outer loop iterations; 100 interalg iterations
    LINEWIDTH = 1.0
    LEGEND_FRAME = True
    USEPGF = False   # set it to true if you want to export diagrams for latex import as .pgf files
    BOOK = False     # set it to true to use the latex backend to produce pdf files
    MAX_ITER = 200                   # maximal number of iterations for solver de
    MAX_ITER_INTERALG = MAX_ITER / 2 # maximal number of iterations for solver interalg
    V_WIND_MIN =   9.0 # minimal wind speed
    V_WIND_MAX =  15.0 # maximal wind speed
    USE_SOLVER_DE = False # the solver de is faster, but less accurate
    OUTER_LOOP = 4 # number of the iterations in the outer loop; should be between 2 (fast) and 8 (accurate)
if SETTINGS == 3:
    # third test case; v_wind = 9.0 .. 15.00; 4 outer loop iterations; 200 interalg iterations
    LINEWIDTH = 1.0
    LEGEND_FRAME = True
    USEPGF = False   # set it to true if you want to export diagrams for latex import as .pgf files
    BOOK = False     # set it to true to use the latex backend to produce pdf files
    MAX_ITER = 200                   # maximal number of iterations for solver de
    MAX_ITER_INTERALG = MAX_ITER     # maximal number of iterations for solver interalg
    V_WIND_MIN =   9.0 # minimal wind speed
    V_WIND_MAX =  15.0 # maximal wind speed
    USE_SOLVER_DE = False # the solver de is faster, but less accurate
    OUTER_LOOP = 4 # number of the iterations in the outer loop; should be between 2 (fast) and 8 (accurate)
if SETTINGS == 4:
    # default settings; v_wind = 2.0 .. 20.00; 4 outer loop iterations; 200 interalg iterations
    LINEWIDTH = 1.0
    LEGEND_FRAME = True
    USEPGF = False   # set it to true if you want to export diagrams for latex import as .pgf files
    BOOK = False     # set it to true to use the latex backend to produce pdf files
    MAX_ITER = 200                   # maximal number of iterations for solver de
    MAX_ITER_INTERALG = MAX_ITER     # maximal number of iterations for solver interalg
    V_WIND_MIN =   2.0 # minimal wind speed
    V_WIND_MAX =  20.0 # maximal wind speed
    USE_SOLVER_DE = False # the solver de is faster, but less accurate
    OUTER_LOOP = 4 # number of the iterations in the outer loop; should be between 2 (fast) and 8 (accurate)


