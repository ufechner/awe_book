# -*- coding: utf-8 -*-
from __future__ import division
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
""" This module provides a scenario object, that can be used to run simulation scenarios
    on the python/ ipython command line. It is indended to be run as main program. """
import numpy as np
from PyQt4 import QtCore

from ModelView import ModelView
from Repository import Repository
import matplotlib
import matplotlib.pyplot as plt

def showWindow(posX, posY):
    print "Backend: ", matplotlib.get_backend()
    if matplotlib.get_backend() == 'Qt4Agg':
        """ Show the currentmatplotlib window at the given position, always on top. """
        manager = plt.get_current_fig_manager()
        manager.window.move(posX, posY)
        manager.window.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
    if matplotlib.get_backend() != 'pgf':
        plt.show(block=False)

class Scenarios(Repository):
    """ Class that provides a scenario object with user-friedly methods to investigate
        different kite-power operation scenarios. """
    def __init__(self):
        Repository.__init__(self)
        self.kite = None
        self.winch = None
        self.model_view = None

    def setKite(self, kitename):
        """ Set the name of the kite, that shall be used for the scenario. """
        if kitename == "":
            self.listKites()
        else:
            self.kite = self.getKite(kitename)

    def setWinch(self, winchname):
        """ Set the name of the winch, that shall be used for the scenario. """
        if winchname == "":
            self.listWinches()
        else:
            self.winch = self.getWinch(winchname)

    def setScenario(self, scenario_number):
        """ Set the number of the scenario, that shall be used. """
        if scenario_number == 1:
            self.setKite('Kite_165_LoD_5_2')
            self.setWinch('EMCE_4000N_8_8')
        if scenario_number == 2:
            self.setKite('Kite_197_LoD_7_2')
            self.setWinch('Winch_12634N_7_14')
        if scenario_number == 3:
            self.setKite('Kite_197_LoD_7_1')
            self.setWinch('Winch_12634N_7_14')
        if scenario_number == 4:
            self.setKite('Kite_139_LoD_7_1')
            self.setWinch('Winch_12634N_7_14')
        if scenario_number == 5:
            self.setKite('Kite_098_LoD_7_1')
            self.setWinch('Winch_12634N_7_14')
        if scenario_number == 6:
            self.setKite('Kite_197_LoD_7_1')
            self.setWinch('Winch_17867N_5_10')
        if scenario_number == 7:
            self.setKite('Kite_197_LoD_7_1')
            self.setWinch('Winch_20000N_4.4_8.8')
        if scenario_number == 8:
            self.setKite('Kite_295_LoD_7_1')
            self.setWinch('Winch_20000N_4.4_8.8')
        if scenario_number == 9:
            self.setKite('Kite_165_LoD_5_2')
            self.setWinch('EMCE_3552N_8_8')


    def createModel(self):
        """ Create a ModelView object using the defined kite and winch. """
        if self.winch and self.kite:
            print "Creating model..."
            if self.kite.name == "Kite_165_LoD_5_2":
                if self.winch.name == "EMCE_3552N_8_8":
                    d_tether = 4.0
                    self.filename1 = self.kite.name + self.winch.name
                else:
                    d_tether = 4.0
                    self.filename1 = "test_02_to_18_17point_Kite_165_LoD_5_2"
            elif self.kite.name == "Kite_197_LoD_7_1":
                if self.winch.name == "Winch_12634N_7_14":
                    d_tether = 7.0 # tether diameter in mm
                    self.filename1 = "test_02_to_18_17point_Kite_197_LoD_7_1_Winch_12634N_7_14"
                elif self.winch.name == "Winch_20000N_4.4_8.8":
                    d_tether = 9.0 # tether diameter in mm
                    self.filename1 = "test_02_to_18_17point_Kite_197_LoD_7_1_Winch_20000N_4.4_8.8"
                else:
                    d_tether = 8.5
                    self.filename1 = "test_02_to_18_17point_Kite_197_LoD_7_1_Winch_17867N_5_10"
            elif self.kite.name == "Kite_295_LoD_7_1":

                    d_tether = 9.0
                    self.filename1 = "test_02_to_18_17point_Kite_295_LoD_7_1_Winch_20000N_4.4_8.8"
            elif self.kite.name == "Kite_139_LoD_7_1":
                d_tether = 7.0 # tether diameter in mm
                self.filename1 = "test_02_to_18_17point_Kite_139_LoD_7_1_Winch_12634N_7_14"
            elif self.kite.name == "Kite_098_LoD_7_1":
                d_tether = 7.0 # tether diameter in mm
                self.filename1 = "test_03_to_18_17point_Kite_098_LoD_7_1_Winch_12634N_7_14"
            else:
                d_tether = 7.0 # tether diameter in mm
                self.filename1 = "test_02_to_18_17point_Kite_197_LoD_7_2_Winch_12634N_7_14"
            phi   = 0 * (np.pi/180)
            delta = 0 * (np.pi/180)
            self.model_view = ModelView(phi, delta, self.kite, self.winch, d_tether)
            # simulation parameters
            self.model_view.max_time =   180 # max time per optimization [s]
            self.model_view.plot     = False
            self.model_view.max_iter = 28000 # max number of iterations per optimization
            self.model_view.f_tol    =    80 # max error w.r.t. the power output [W]
            return True
        elif self.winch is None:
            print "Define a winch with scen.setWinch(<winchname>) first!"
            print "Valid winch names:"
            print self.listWinches()
        elif self.kite is None:
            print "Define a kite with scen.setKite(<kitename>) first!"
            print "Valid kite names:"
            print self.listKites()
        return False

    def simulatePower(self, high_res = False, test = False):
        """ Simulate the power output as function of the wind-speed and store the results
            in a .json file.

            **Parameters**

            :param high_res: Pass True to add additional wind speeds to the simulation.
        """
        if self.createModel():
            self.model_view.simulatePowerVsWind(self.filename1, high_res, test)

    def calcEta(self, v_wind_ground = 8.0, delta_length = 200.0):
        """ calculate eta_p, eta_eo and eta_ei for a given wind speed """

        if self.createModel():
            self.model_view.optimizeFullCycle(v_wind_ground, delta_length)

    def plotPower(self):
        if self.createModel():
            fig = self.model_view.plotPowerVsWind(self.filename1)
            showWindow(640 * 2, 0)
            return fig

    def plotMechPower(self):
        if self.createModel():
            fig = self.model_view.plotMechPowerVsWind(self.filename1)
            showWindow(640 * 2, 574)
            return fig

    def plotSpeed(self):
        if self.createModel():
            fig = self.model_view.plotSpeedVsWind(self.filename1)
            showWindow(0, 574)
            return fig

    def plotTether(self):
        if self.createModel():
            fig = self.model_view.plotTetherLenghtAndHeightVsWind(self.filename1)
            showWindow(640, 0)
            return fig

    def plotEff(self):
        if self.createModel():
            fig = self.model_view.plotEfficiencies(self.filename1)
            showWindow(0, 0)
            #return fig

    def plotWindSpeed(self):
        if self.createModel():
            fig = self.model_view.plotWindSpeed(self.filename1)
            showWindow(640, 574)
            return fig

    def plotAll(self):
        if self.createModel():
            self.plotPower()
            self.plotMechPower()
            self.plotSpeed()
            self.plotTether()
            self.plotEff()
            self.plotWindSpeed()

    def greeting(self):
        print "\nType one or more of the following commands:"
        print "-------------------------------------------"
        print "scen.setKite('<kitename>')"
        print "scen.setWinch('<winchname>')"
        print "scen.setScenario(<scenario_number>)"
        print "scen.listKites()"
        print "scen.listWinches()"
        print "fig = scen.plotPower()"
        print "fig = scen.plotEff()"
        print "fig = scen.plotSpeed()"
        print "fig = scen.plotTether()"
        print "scen.plotAll()"
        print "scen.simulatePower()"
        print "scen.greeting()"
        print "scen.help()\n"

    def help(self):
        print "Change y scaling example, plot with one axis:"
        print ">>> plt.ylim(160,610)"
        print ">>> fig.canvas.draw()"
        print
        print "Change y scaling example, plot with two axis:"
        print ">>> plt.ax1.set_ylim(150,600)"
        print ">>> fig.canvas.draw()"

if __name__ == "__main__":
    ## create scenario object
    scen = Scenarios()
    scen.greeting()
