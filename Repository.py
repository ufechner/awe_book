# -*- coding: utf-8 -*-
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University 
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""    
""" Defines the class Repository that contains any number of kite and groundstation models 
    that can be used in simulation scenarios. """
from Framework import SimpleKite, SimpleWinch, Generator, Motor

class Repository(object):
    def __init__(self):
        self.kites = []
        self.kites.append("Kite_165_LoD_5_2")
        self.kites.append("Kite_197_LoD_7_1")
        self.kites.append("Kite_197_LoD_7_2")
        self.kites.append("Kite_139_LoD_7_1")
        self.kites.append("Kite_098_LoD_7_1")
        self.kites.append("Kite_295_LoD_7_1")
        self.winches = []
        self.winches.append("EMCE_4000N_8_8")
        self.winches.append("Winch_12634N_7_14")
        self.winches.append("Winch_17867N_5_10")
        self.winches.append('Winch_20000N_4.4_8.8')
        
    def getKite(self, kite_name):
        if kite_name == "Kite_165_LoD_5_2":
            ## define simple kite                
            area   = 16.5   # effective area [m²]
            paramCL = 1.0   # lift coefficient
            paramLoD = 5.0  # lift over drag ratio during reel-out
            reelInLoD = 2.0 # lift over drag ration during reel-in
            reelInCD = 0.07 # Bryan used 0.07, but I calculated 0.1118. What is better?   
            mass   = 20.0   # includes mass of pod, sensorunit and bridle [kg]
        elif kite_name == "Kite_197_LoD_7_2":
            ## define a better (future) kite
            paramCL  = 1.0
            paramLoD = 7.0
            reelInLoD = 2.0
            reelInCD = 0.14 # xx used 0.07 better 0.1118?
            area     = 19.7
            mass     = 30.0
        elif kite_name == "Kite_197_LoD_7_1":
            ## define a better (future) kite
            paramCL  = 1.0
            paramLoD = 7.0
            reelInLoD = 1.0
            reelInCD = 0.14 # xx used 0.07 better 0.1118?
            area     = 19.7
            mass     = 30.0
        elif kite_name == "Kite_139_LoD_7_1":
            ## define a better (future) kite
            paramCL  = 1.0
            paramLoD = 7.0
            reelInLoD = 1.0
            reelInCD = 0.14 # xx used 0.07 better 0.1118?
            area     = 13.9
            mass     = 25.0            
        elif kite_name == "Kite_098_LoD_7_1":
            ## define a better (future) kite
            paramCL  = 1.0
            paramLoD = 7.0
            reelInLoD = 1.0
            reelInCD = 0.14 # xx used 0.07 better 0.1118?
            area     = 9.85
            mass     = 25.0           
        elif kite_name == "Kite_295_LoD_7_1":
            ## define a better (future) kite
            paramCL  = 1.0
            paramLoD = 7.0
            reelInLoD = 1.0
            reelInCD = 0.14 # xx used 0.07 better 0.1118?
            area     = 29.55
            mass     = 43.0            
        else:
            print "Unknown kite name: ", kite_name
        kite = SimpleKite(mass, area, paramCL, paramLoD, reelInLoD, reelInCD, kite_name)    
        return kite
        
    def listKites(self):
        return self.kites
        
    def getGenerator(self, gen_name):
        if gen_name == '500STK8M':
            # max torque with 32 cm drum: 1824 Nm at 352 rpm rated torque 1250 Nm 1500 Nm might be ok
            ampere_per_nm = 0.08563 # input torque at rated speed 1197 Nm 400rpm 102.5 A
            resistance = 0.04 # Ohm per winding
            ratio_k = 1.2     # ratio of total electric losses and DC copper losses
            tau_c = 66.8      # coulomb friction [Nm]
            c_viscous_friction =  0.0  # viscous friction coefficient [Nms]
        elif gen_name == 'ECME_20kW':
            ampere_per_nm = 0.0383 # nominell torque at drum 755.2 Nm without the gearbox friction
            resistance = 9.02 / 3.0 / 1.5 # Ohm per winding
            ratio_k = 1.5                 # ratio of total electric losses and DC copper losses
            tau_c = 19.54                 # coulomb friction [Nm]
            c_viscous_friction =  0.7978  # viscous friction coefficient [Nms]
        else:
            print "Unknown generator name: ", gen_name
            return None
        generator = Generator(ampere_per_nm, resistance, ratio_k, tau_c, c_viscous_friction)
        return generator
        
    def getMotor(self, motor_name):
        if motor_name == '500STK8M':
            ampere_per_nm = 0.08563
            resistance =  0.04   # Ohm per winding
            ratio_k    =  1.2    # ratio of total electric losses and DC copper losses
            tau_c      = 66.8    # coulomb friction [Nm]
            c_viscous_friction =  0.0  # viscous friction coefficient [Nms]
        elif motor_name == '300STK8M':              
            # Assumtion: 300 STK 8M with 700 rpm and 270 Nm torque  
            # Pnenn: 17399 Wel, 800 rpm, 226 Nm, losses: 8% = 1289 W, 42.9A            
            ampere_per_nm =  0.1898  # A/Nm
            resistance    =  0.08    # Ohm per winding
            ratio_k       =  1.35    # ratio of total electric losses and DC copper losses
            tau_c         = 14.32    # coulomb friction [Nm]
            c_viscous_friction =  0.0  # viscous friction coefficient [Nms]
        elif motor_name == 'ECME_20kW':
            ampere_per_nm = 0.0383 # nominell torque at drum 755.2 Nm without the gearbox friction
            resistance = 9.02 / 3.0 / 1.5 # Ohm per winding
            ratio_k = 1.5                 # ratio of total electric losses and DC copper losses
            tau_c = 19.54                 # coulomb friction [Nm]    
            c_viscous_friction =  0.7978  # viscous friction coefficient [Nms]
        else:
            print "Unknown motor name: ", motor_name
            return None
        motor = Motor(ampere_per_nm, resistance, ratio_k, tau_c, c_viscous_friction)
        return motor        
        
    def getWinch(self, winch_name):
        if winch_name == "EMCE_4000N_8_8":
            v_reel_out_max = 8.0 # [m/s]
            crest_factor_reelin_speed = 1.1
            v_reel_in_max =  8.0 / crest_factor_reelin_speed # [m/s]
            force_max = 4000.0   # [N]
            p_el_nom = 20040.0   # nominall electrical generator power [W]
            eta_generator = 0.8
            diameter = 0.323       
            generator = self.getGenerator('ECME_20kW')
            motor     = self.getMotor('ECME_20kW')
        elif winch_name == "EMCE_3552N_8_8":
            v_reel_out_max = 5.0*1.2 # [m/s]
            crest_factor_reelin_speed = 1.1
            v_reel_in_max =  8.0 / crest_factor_reelin_speed # [m/s]
            force_max = 3200.0*1.11   # [N]
            p_el_nom = 20040.0   # nominall electrical generator power [W]
            eta_generator = 0.8
            diameter = 0.323       
            generator = self.getGenerator('ECME_20kW')
            motor     = self.getMotor('ECME_20kW')
        elif winch_name == "Winch_12634N_7_14":
            v_reel_out_max = 7.0
            crest_factor_reelin_speed = 1.1
            v_reel_in_max = 14.0 / crest_factor_reelin_speed # [m/s]
            force_max = 12634.0
            p_el_nom = 53500.0
            eta_generator = 0.905 # efficiency at 333 rpm of 75% generator and 25% motor
            diameter = 0.332
            generator = self.getGenerator('500STK8M')
            motor = self.getMotor('300STK8M')
        elif winch_name == "Winch_17867N_5_10":
            v_reel_out_max = 4.95
            crest_factor_reelin_speed = 1.1
            v_reel_in_max = 9.9 / crest_factor_reelin_speed # [m/s]
            force_max = 17867.0
            p_el_nom = 53500.0
            eta_generator = 0.905 # efficiency at 333 rpm of 75% generator and 25% motor
            diameter = 0.235
            generator = self.getGenerator('500STK8M')
            motor = self.getMotor('300STK8M')
            # winch 4.42 / 8.84 m/s   20000 N 9mm    d=0.21 m
        elif winch_name == "Winch_20000N_4.4_8.8":
            # nominal 335 rpm, 2100 Nm torque
            # torque generator: 1575 N + torque motor: 525 N
            # p_mech generator reel_out: 527625 W 
            # p_el generator   reel_out: 477501 W
            v_reel_out_max = 4.42
            crest_factor_reelin_speed = 1.1
            v_reel_in_max = 8.84 / crest_factor_reelin_speed # [m/s]
            force_max = 20000.0
            p_el_nom = 53500.0
            eta_generator = 0.905 # efficiency at 333 rpm of 75% generator and 25% motor
            diameter = 0.21
            generator = self.getGenerator('500STK8M')
            motor = self.getMotor('300STK8M')

        else:
            print "Unknown winch name: ", winch_name
            return None
        winch = SimpleWinch(v_reel_out_max, v_reel_in_max, force_max, p_el_nom, diameter, eta_generator, generator, motor, 
                            winch_name)
        return (winch)
        
    def listWinches(self):
        return self.winches

if __name__=='__main__':
    repo = Repository()
    kite = repo.getKite('Kite_197_LoD_7_2')
    print kite.json()
    winch = repo.getWinch('Winch_12634N_7_14')
    print winch.json()
