# -*- coding: utf-8 -*-
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University 
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""    
""" This module implements the common base class Serialize and the 
    commonly used function drange. """
import simplejson as json
from collections import OrderedDict
import webbrowser
import os

def setAxLinesBW(ax):
    """
    Take each Line2D in the axes, ax, and convert the line style to be 
    suitable for black and white viewing.
    """
    MARKERSIZE = 3

    COLORMAP = {
        'blue': {'marker': None, 'dash': (None,None)},
        'green': {'marker': None, 'dash': [5,5]},
        'red': {'marker': None, 'dash': [2,3]},
        'black': {'marker': None, 'dash': [5,3,1,3]},
        'm': {'marker': None, 'dash': [5,2,5,2,5,10]},
        'yellow': {'marker': None, 'dash': [5,3,1,2,1,10]},
        'k': {'marker': 'o', 'dash': (None,None)} #[1,2,1,10]}
        }

    for line in ax.get_lines():
        origColor = line.get_color()
        if origColor != 'black':
            line.set_color('black')
            line.set_dashes(COLORMAP[origColor]['dash'])
            line.set_marker(COLORMAP[origColor]['marker'])
            line.set_markersize(MARKERSIZE)

def setFigLinesBW(fig):
    """
    Take each axes in the figure, and for each line in the axes, make the
    line viewable in black and white.
    """
    for ax in fig.get_axes():
        setAxLinesBW(ax)

def createDir(path):
    """ Create a directory recursivly. Parameter: Full path. """
    import os.path as os_path
    paths_to_create = []
    while not os_path.lexists(path):
        paths_to_create.insert(0, path)
        head, tail = os_path.split(path)
        if len(tail.strip()) == 0: # Just incase path ends with a / or \
            path = head
            head, tail = os_path.split(path)
        path = head

    for path in paths_to_create:
        os.mkdir(path)

def drange(start, stop, step):
    """ This method can be used for iterating over float values. """
    var = start
    while round(var, 7) <= round(stop, 7):
        yield var
        var += step

def convert_deep(obj):    
    """ This function creates an ordered dictionary of the object members. 
    It also iterates over child objects that start with an underscore and adds 
    the entry CLASS with the name of the class at the top. """
    objmembers = dict((k, v) for (k, v) in obj.__dict__.iteritems() if k.startswith('_'))
    objmembers['CLASS'] = obj.__class__.__name__
    result = OrderedDict(sorted(objmembers.items(), key=lambda t: t[0]))
    result.update(OrderedDict(sorted(obj.__dict__.items(), key=lambda t: t[0])))
    return result
    
def convert_flat(obj):   
    """ This function creates an ordered dictionary of the object members. 
    It removes members that start with an underscore and adds 
    the entry CLASS with the name of the class at the top. """
    objmembers = OrderedDict()
    objmembers['CLASS'] = obj.__class__.__name__
    temp = dict((k, v) for (k, v) in obj.__dict__.iteritems() if not k.startswith('_'))    
    objmembers.update(OrderedDict(sorted(temp.items(), key=lambda t: t[0])))
    return objmembers

class Serialize(object):
    """ This class implements the methods, that are needed by all storage classes of the
        kite-power system simulation program.
        In addition it provides the method help() that displays the related wiki page
        in firefox. """
    def update(self, newValuesDict):
        """ update the attributes of the current class with the values from the newValuesDict,
        but only those that do not start with an underscore (no objects)  """
        newAttributes = dict((k, v) for (k, v) in newValuesDict.iteritems() if not k.startswith('_')) 
        self.__dict__.update(newAttributes)
    def json(self, deep=False):
        """ serialize the object in human readable json format """
        if deep:
            return json.dumps(self, default=convert_deep, indent=4)
        else:
            return json.dumps(self, default=convert_flat, indent=4)
    def help(self):
        """ show the webpage, stored in the attribute wiki using firefox """
        webbrowser.get('firefox').open_new_tab(self.wiki)       
    def save(self, filename='', deep=False):
        """ Save the object in a file with the given name. If no filename is specified the 
        name attribute of the object will be used as filename (suffix: .json) """
        if filename == '':
            filename = self.name
        handle = open('./models/'+filename+'.json', 'w')
        handle.write(self.json(deep))
        handle.close()       
        print "Saved file with name: " + filename + '.json'
    def load(self, filename):
        """ Populate the attributes of the object with the data from the given file name. """
        handle = open('./models/'+filename+'.json', 'r')
        self.update(json.loads(handle.read()))
        print "Loaded model with name: " + self.name
        handle.close()
        