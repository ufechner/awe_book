# -*- coding: utf-8 -*-
# pylint: disable-msg = W0621
from __future__ import division
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
View on a model for calculating the optimal operating conditions of a kite power system based on
the averaging framework from Micheal Noom. Defines the class ModelView. It contains a method to
do run a simulation over a given range of wind speeds that saves the result in .json format.
Additionally it contains methods for creating different diagrams from the result file.
"""
import matplotlib as mpl
import numpy as np
from math import sin, cos, pi
#from Weibull import pdf, scale
from math import exp, gamma
from BaseClasses import createDir, setFigLinesBW
from matplotlib.patches import FancyArrowPatch
from matplotlib.patches import Wedge
from Settings import LINEWIDTH, USEPGF, BOOK, V_WIND_MIN, V_WIND_MAX

import jsonpickle
from os.path import expanduser

if USEPGF:
    mpl.use('pgf')
    pgf_with_rc_fonts = {
        "font.family": "serif",
        "font.serif": [],                   # use latex default serif font
    }

    pgf_with_custom_preamble = {
        "pgf.preamble": [
             r"\usepackage{gensymb}",         # load additional packages
             ]
    }
    mpl.rcParams.update(pgf_with_custom_preamble)

    mpl.rcParams.update({'font.size': 9})
    mpl.rcParams.update(pgf_with_rc_fonts)
    mpl.rc('xtick', labelsize=9)
    mpl.rc('ytick', labelsize=9)
    mpl.rcParams['axes.labelsize'] = '10'
else:
    mpl.rc('xtick', labelsize=12)
    mpl.rc('ytick', labelsize=12)
    if BOOK:
        mpl.rcParams.update({"font.family": "serif"})
        mpl.rcParams.update({"text.usetex" : "True"})
        mpl.rcParams.update({'font.size': 12})
        mpl.rc('xtick', labelsize=13)
        mpl.rc('ytick', labelsize=13)
    mpl.rcParams['axes.labelsize'] = '14'

import matplotlib.pyplot as plt
from Model import Model

SHAPE = 2.0 # form of the Weibull distribution

# cumunative weibull distribution function
def cdf(v_wind, scale):
    return 1 - exp(-(v_wind / scale)**SHAPE)

# probability distribution function
def pdf1(v_wind, scale):
    return SHAPE * (v_wind**(SHAPE-1.0)) / (scale**SHAPE)  * exp(-(v_wind / scale)**SHAPE)

# discrete probability distribution function
def pdf(v_wind, delta, scale):
    return(cdf(v_wind + delta / 2.0, scale) - cdf(v_wind-delta / 2.0, scale))

def mean(scale):
    return scale * gamma((SHAPE + 1.0) / SHAPE)

def scale(mean):
    return mean / gamma((SHAPE + 1.0) / SHAPE)

def plotTrajectory():
        """ Plot the reel-in trajectory. """
        from Repository import Repository
        from Framework import Framework
        #legend([plot1], "title", prop = fontP)
        repo = Repository()
        kite = repo.getKite('Kite_295_LoD_7_1')
        winch = repo.getWinch('Winch_20000N_4.4_8.8')
        ## define operational parameters
        beta_ro  = 20.728515625 * (np.pi/180)
        phi   = 0 * (np.pi/180)
        delta = 0 * (np.pi/180)
        ## create framework object
        framework = Framework(phi, delta, kite, winch)
        max_tether_length = 317.9654768514668 + 100
        reel_out_time = 58.53384661076482
        reel_out_power = 2900230.215647403 / reel_out_time
        delta_length = 200.0
        v_wind = 7.0
        framework.reelIn(max_tether_length, beta_ro, reel_out_time, reel_out_power, delta_length, v_wind, False)
        # list of points for the intermediate phase
        #intermediate_beta = [framework.beta_list[-1], beta_ro]
        intermediate_beta, intermediate_length = [], []
        #intermediate_length = [max_tether_length - delta_length, max_tether_length - delta_length]
        for beta in np.linspace(framework.beta_list[-1], beta_ro):
            intermediate_beta.append(beta)
            intermediate_length.append(max_tether_length - delta_length)


        # list of points for the reel-out phase
        ro_beta = [beta_ro, beta_ro]
        ro_length = [max_tether_length - delta_length, max_tether_length]
        x_list_i, y_list_i  = [], []
        x_list_ro, y_list_ro = [], []
        i = 0
        for beta in intermediate_beta:
            x_list_i.append(intermediate_length[i] * cos(beta))
            y_list_i.append(intermediate_length[i] * sin(beta))
            i += 1
        i = 0
        for beta in ro_beta:
            x_list_ro.append(ro_length[i] * cos(beta))
            y_list_ro.append(ro_length[i] * sin(beta))
            i += 1
        # plot XY diagram
        width = 3.95
        scale1 = 1.0
        if USEPGF:
            plt.figure(1, figsize=(width, 2.7*250/400))
            right=0.90-1/width
            bottom=0.17
            anchor=1.9
        else:
            mpl.rc('xtick', labelsize=9)
            mpl.rc('ytick', labelsize=9)
            mpl.rcParams['axes.labelsize'] = '10'
            scale1 = 1.7
            right = 0.90-1/2.8
            bottom = 0.18
            plt.figure(1, figsize=(width*scale1, 2.2*250/400*scale1))
            anchor = 1.9
        plt.subplots_adjust(left=0.15,bottom=bottom,right=right,top=0.90)
        plt.gca().grid(False)
        plt.gca().set_autoscale_on(False)
        x_list = []
        y_list = []
        i = 0
        for beta in framework.beta_list:
            x_list.append(framework.length_list[i] * cos(beta))
            y_list.append(framework.length_list[i] * sin(beta))
            i += 1
        plt.gca().set_ylabel('Height [m]')
        plt.gca().set_xlabel('x [m]')
        plt.ylim(0.0, 250.0) # Set y limits
        plt.xlim(0.0, 400.0) # Set y limits
        plt.gca().add_patch(FancyArrowPatch((260,212),(250,213.5), arrowstyle='->', mutation_scale=20))
        # draw a line the origin to the kite and display the elevation angle beta
        plt.plot([0,260],[0, 212], color='k', ls=':')
        w1 = Wedge((0,0), 50, 0, 39.25, fc='w')
        plt.gca().add_artist(w1)
        plt.figtext(0.22, 0.2,  r"$\beta$", size='large')
        # plot the trajectory
        line1 = plt.plot(x_list, y_list, 'k', label = "reel-in      phase")
        line2 = plt.plot(x_list_i, y_list_i, 'k:', label = "transition phase")
        line3 = plt.plot(x_list_ro, y_list_ro, 'k--', dashes=(4,4), label = "reel-out    phase")
        lines = line1 + line2 + line3
        plt.gca().legend(lines, [l.get_label() for l in lines],  loc='right', bbox_to_anchor=(anchor, 0.5), prop={'size':9})
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/' + 'trajectory')
        if USEPGF:
            plt.savefig(base_file_name + '.pgf')
            print "Written file: "+ base_file_name + '.pgf'
        else:
            plt.savefig('./results/'+'trajectory'+'.pdf')
        print "Finshed!"
        if not USEPGF:
            plt.show()

class ModelView(Model):
    """
    Class for plotting the optimal operating conditions of a kite power system based on
    the averaging framework from Micheal Noom.
    """

    def simulatePowerVsWind(self, filename, high_res = False, test = False, delta_length = 200.0):
        """ Simulate the power output as function of the wind-speed and store the results
            in a .json file.
        """
        print "Starting simulation..."
        print "Expected duration: 17 minutes on a Core i7, 3.4 Ghz"
        results = []
        last_beta = None
        last_lav = None
        last_rel_speed = None
        if self.area < 10:
            v_start = 3.0
            points = 14
        else:
            v_start = 2.0
            points = 15
        wind_speeds = []
        if not test:
            wind_speeds.extend(np.linspace(v_start, 16.0, points))
        else:
            wind_speeds.append(8.0)
        # add same extra windspeeds where a simulation shall be done
        if high_res:
            wind_speeds.append(4.7)
            wind_speeds.append(5.2)
            wind_speeds.append(6.6)
            wind_speeds.append(6.8)
            wind_speeds.sort()
        for v_wind_ground in wind_speeds:
            if v_wind_ground < V_WIND_MIN or v_wind_ground > V_WIND_MAX:
                continue
            result = self.optimizeFullCycle(v_wind_ground, delta_length, last_beta = last_beta,
                                             last_lav = last_lav, last_rel_speed = last_rel_speed)
            results.append(result)
            last_beta = result['beta_opt_degree']
            last_lav = result['l_av_opt']
            last_rel_speed = result['rel_speed_opt']
            last_beta *= np.pi/180.0
        jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)
        results_str = jsonpickle.encode(results)
        fileObj = open('./results/'+filename+'.json',"w") # open for for write
        fileObj.write(results_str)
        fileObj.close()
        print "Finshed simulation and saved results!"

    def createMeasuredValuesLists(self):
        """
        Create two lists of windspeed and mechanical power values. Source: MSc Thesis
        from Marien Ruppert, TU Delft, 2012, page 44.
        """
        listX1, listPM1 = [], []
        listX1.append(2.8); listPM1.append(1.79e3)
        listX1.append(4.4); listPM1.append(2.73e3)
        listX1.append(6.6); listPM1.append(4.44e3)
        listX1.append(7.4); listPM1.append(4.80e3)
        listX1.append(8.0); listPM1.append(5.19e3)
        listX1.append(10.0); listPM1.append(5.75e3)

        # calculate the windspeed in 10m height from the measured values in 6m height
        for index, speed6m in enumerate(listX1):
            listX1[index] = self.calcWindHeight(speed6m, 10.0, 6.0)

        return listX1, listPM1

    def plotPowerVsWind(self, filename):
        """
        Read simulation result from file (.json format) and
        plot the elevation angle beta and the power output as a function of the windspeed.
        """
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, listPM, listPE, listB) = ([], [], [], [])
        av_power = 0.0
        mean = 5.5 # average wind speed at 10 m height
        for result in results:
            v_wind_ground = result['v_wind_ground']
            net_elec_power = result['net_elec_power']
            # calculate the integrated wind power for steps of 1 m/s using the weibull distribution
            if abs(v_wind_ground - round(v_wind_ground)) < 0.05:
                av_power += net_elec_power * pdf(v_wind_ground, 1.0, scale(mean))
            listX.append(result['v_wind_ground'])
            listPM.append(result['netMechPower'])
            listPE.append(result['net_elec_power'])
            listB.append(result['beta_opt_degree'])
        print "Average power at 5.5 m/s mean wind speed: ", av_power
        if USEPGF:
            if self.kite.name == 'Kite_165_LoD_5_2':
                self.fig1 = plt.figure(1, figsize=(4.8, 2.8))
            else:
                self.fig1 = plt.figure(1, figsize=(4.8 * 0.9 , 2.8 * 0.85))
        else:
                if BOOK:
                    mpl.rcParams.update({'font.size': 11})
                    mpl.rc('xtick', labelsize=12)
                    mpl.rc('ytick', labelsize=12)
                    mpl.rcParams['axes.labelsize'] = '12'
                    self.fig1 = plt.figure(5, figsize=(4.8*1.2, 3.5*1.2*0.933*0.908))
                else:
                    self.fig1 = plt.figure(1)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel('Average power [W]')
        line1 = plt.plot(listX, listPM, linewidth = LINEWIDTH, color="blue", label="Mech. power (sim.)")
        line3 = plt.plot(listX, listPE, linewidth = LINEWIDTH, color="green", label="Elec. power (sim.)")
        if self.kite.name == 'Kite_165_LoD_5_2':
            plt.ylim(-2000.0, 8000.0) # Set y limits
            listX1, listPM1 = self.createMeasuredValuesLists()
            line4 = plt.plot(listX1, listPM1, 'x', markersize=7, markeredgewidth = 1.5, color="black",
                             label="Mech. power (exp.)")
        else:
            line4 = []
        plt.ax2 = plt.twinx()
        plt.ax2.set_ylabel(r'Elevation angle $\beta$ [$^\circ$]')
        if self.kite.name == 'Kite_165_LoD_5_2':
            plt.ylim(0.0, 80.0) # Set y limits
        else:
            plt.ylim(10.0, 80.0) # Set y limits
        line2 = plt.plot(listX, listB, linewidth = LINEWIDTH, color="red", label=r"Elevation angle (sim.)")
        plt.ax1.set_xlabel('Wind speed at 10 m height [m/s]')
        lines = line1 + line3 + line2 + line4
        if USEPGF:
            setFigLinesBW(self.fig1)
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right', prop={'size':9})
        else:
            if BOOK:
                setFigLinesBW(self.fig1)
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right', prop={'size':12})
        plt.ax1.grid(True, color='0.5')
        self.fig1.tight_layout()
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/power_' + self.winch.name)
        if USEPGF:
            plt.savefig(base_file_name + '.pgf')
            print "Written file: "+ base_file_name + '.pgf'
        plt.savefig('./results/'+filename+'_power.png')
        print "Finshed!"
        return self.fig1

    def plotMechPowerVsWind(self, filename):
        """
        Read simulation result from file (.json format) and
        plot the elevation angle beta and the power output as a function of the windspeed.
        """
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, listPM, listPMO, listPMI) = ([], [], [], [])
        for result in results:
            listX.append(result['v_wind_ground'])
            listPM.append(result['netMechPower'])
            listPMO.append(result['reelOutEnergy'] / result['reel_out_time'])
            listPMI.append(-result['reelInEnergy'] / result['reel_in_time'])
        if USEPGF:
            self.fig2 = plt.figure(2, figsize=(4.8, 3.5))
        else:
            self.fig2 = plt.figure(2)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel('Average mechanical power [W]')
        line1 = plt.plot(listX, listPM, linewidth = LINEWIDTH, color="blue", label="Mechanical cycle power [W]")
        line4 = plt.plot(listX, listPMO, linewidth = LINEWIDTH, color="yellow", label="Mechanical reel-out power [W]")
        line5 = plt.plot(listX, listPMI, linewidth = LINEWIDTH, color="black", label="Mechanical reel-in power [W]")
        plt.ax1.set_xlabel('Wind speed at 10 m height [m/s]')
        lines = line1 + line4 + line5
        if USEPGF:
            setFigLinesBW(self.fig2)
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':8})
        else:
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':11})
        plt.ax1.grid(True, color='0.5')
        self.fig2.tight_layout()
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        print "Winchname: ", self.winch.name
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/power_' + self.winch.name)
        if USEPGF:
            plt.savefig(base_file_name + '.pgf')
            print "Written file: "+ base_file_name + '.pgf'
        plt.savefig('./results/'+filename+'_mech_power.png')
        print "Finshed!"
        return self.fig2


    def plotSpeedVsWind(self, filename, delta_length = 200.0):
        """
        Read simulation result from file (.json format) and
        plot the reel-in and reel-out times as a function of the windspeed.
        """
        print "plotSpeedVsWind..."
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, list_v_ri, list_v_ro, list_duty_cycle) = ([], [], [], [])
        for result in results:
            listX.append(result['v_wind_ground'])
            list_v_ro.append(delta_length / result['reel_out_time'])
            list_v_ri.append(delta_length / result['reel_in_time'])
            list_duty_cycle.append(100.0 * result['reel_out_time'] \
                                         / (result['reel_out_time'] + result['reel_in_time']))
        if USEPGF:
            self.fig3 = plt.figure(3, figsize=(4.8/1.085, 3.5/1.2))
        else:
            self.fig3 = plt.figure(3)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel('Reelin/ reelout velocity [m/s]')
        vmax = self.winch.v_reel_in_max * 1.1
        print "vmax, delta_length", vmax, delta_length
        print list_v_ro
        plt.ylim(0.0, vmax)
        line1 = plt.plot(listX, list_v_ri, linewidth = LINEWIDTH, color="blue", label="Reel-in velocity $v_{t,i}$")
        line2 = plt.plot(listX, list_v_ro, linewidth = LINEWIDTH, color="red", label="Reel-out velocity $v_{t,o}$")

        plt.ax2 = plt.twinx()
        plt.ax2.set_ylabel('Duty cycle [%]')
        if abs(vmax - 8.84) < 0.1:
            plt.ylim(0.0, 88.4) # Set y limits
        else:
            plt.ylim(20.0, 90.0) # Set y limits
        line3 = plt.plot(listX, list_duty_cycle, linewidth = LINEWIDTH, color="green", label="Duty-cycle $D$")
        plt.ax1.set_xlabel('Wind speed at 10 m height [m/s]')
        # draw a default vline at x=1 that spans the yrange
        if abs(vmax - 8.84) < 0.1:
            plt.figtext(0.18+0.05, 0.53,  "I", size='xx-large')
            plt.axvline(x= 5.2, color="black")
            plt.figtext(0.30+0.05, 0.53,  "II", size='xx-large')
            plt.axvline(x= 6.8, color="black")
            plt.figtext(0.44+0.05, 0.53,  "III", size='xx-large')
        lines = line1 + line2 + line3
        if USEPGF:
            setFigLinesBW(self.fig3)
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right', prop={'size':9})
        else:
            plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right', prop={'size':11})
        plt.ax1.grid(True, color='0.5') # grey

        self.fig3.tight_layout()
        plt.subplots_adjust(left=0.138, right = 0.95)
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        print "Winchname: ", self.winch.name
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/speed_' + self.winch.name)
        plt.savefig('./results/'+filename+'_speed.png')
        if USEPGF:
            plt.savefig(base_file_name + '.pgf')
            print "Written file: "+ base_file_name + '.pgf'
        print "Finshed!"
        return self.fig3

    def plotTetherLenghtAndHeightVsWind(self, filename):
        """
        Read simulation result from file (.json format) and
        plot the reel-in and reel-out times as a function of the windspeed.
        """
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, list_height, list_tether_length) = ([], [], [])
        for result in results:
            listX.append(result['v_wind_ground'])
            list_height.append(sin(result['beta_opt_degree'] * np.pi /180.0) * result['l_av_opt'])
            list_tether_length.append(result['l_av_opt'])
        self.fig4 = plt.figure(4)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel('Average height [m]')
        line1 = plt.plot(listX, list_height, linewidth = LINEWIDTH, color="blue", label="Average height [m]")
        line2 = plt.plot(listX, list_tether_length, linewidth = LINEWIDTH, color="red",
                         label="Average tether length [m]")
        plt.ax2 = plt.twinx()
        plt.ax2.set_ylabel('Average tether length [m]')
        plt.ylim(180.0, 620.0) # Set y limits
        plt.ax1.set_xlabel('Wind speed at 10 m height [m/s]')
        lines = line1 + line2
        plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right')
        plt.ax1.grid(True, color='0.5')
        self.fig4.tight_layout()
        plt.savefig('./results/'+filename+'_height.png')
        print "Finshed!"
        self.fig4.show()
        return self.fig4

    def plotEfficiencies(self, filename):
        """
        Read simulation result from file (.json format) and
        plot the efficencies as a function of the windspeed.
        """
        print "Starting plotEfficiencies..."
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, list_eta_pumping, list_eta_cycle, list_duty_cycle, list_eta_elec, list_eta_tot) \
                                                                                    = ([], [], [], [], [], [])
        for result in results:
            listX.append(result['v_wind_ground'])
            duty_cycle =  result['reel_out_time'] / (result['reel_out_time'] + result['reel_in_time'])
            eta_pumping = (result['reelOutEnergy'] + result['reelInEnergy']) / result['reelOutEnergy']
            power_electric = result['net_elec_power']
            eta_elec = power_electric / result['netMechPower']
            #print result['netMechPower'], result['reel_out_speed'], power_electric, eta_elec
            list_eta_pumping.append(100.0 * eta_pumping)
            list_eta_cycle.append(100.0 * eta_pumping * duty_cycle)
            list_duty_cycle.append(100.0 * duty_cycle)
            list_eta_elec.append(100.0 * eta_elec)
            list_eta_tot.append(100.0 * eta_elec * eta_pumping * duty_cycle)

        if USEPGF:
            if self.winch.name == "Winch_20000N_4.4_8.8":
                self.fig5 = plt.figure(5, figsize=(4.8/1.09, 3.5/1.25))
            else:
                self.fig5 = plt.figure(5, figsize=(4.8/1.09, 3.5/1.2))
        else:
            if BOOK:
                if self.winch.name == "Winch_20000N_4.4_8.8":
                    mpl.rcParams.update({'font.size': 11})
                    mpl.rc('xtick', labelsize=12)
                    mpl.rc('ytick', labelsize=12)
                    mpl.rcParams['axes.labelsize'] = '12'
                    self.fig5 = plt.figure(5, figsize=(4.8*1.2, 3.5*1.2*0.933))
                else:
                    self.fig5 = plt.figure(5, figsize=(4.8*1.5, 3.5*1.5))
            else:
                self.fig5 = plt.figure(5)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel(u'Efficiency/ duty cycle [\%]')
        plt.ylim(0.0, 100.0) # Set y limits
#
        line1 = plt.plot(listX, list_eta_pumping, linewidth = LINEWIDTH, color="blue", label="Pumping eff. $\eta_p$")
        line2 = plt.plot(listX, list_duty_cycle, linewidth = LINEWIDTH, color="green", label="Duty-cycle $D$")
        line3 = plt.plot(listX, list_eta_cycle, linewidth = LINEWIDTH, color="red", label="Cycle eff. $\eta_{cyc}$")
        line4 = plt.plot(listX, list_eta_elec, linewidth = LINEWIDTH, color="m", label="Electrical eff. $\eta_e$")
        line5 = plt.plot(listX, list_eta_tot, linewidth = LINEWIDTH, color="yellow", label="Total eff. $\eta_{tot}$")
        plt.ax1.set_xlabel('Wind speed at 10 m height [m/s]')
        lines = line1 + line2 + line3 + line4 + line5
        if USEPGF:
            setFigLinesBW(self.fig5)
            if self.winch.name == "Winch_20000N_4.4_8.8":
                plt.ax1.legend(lines, [l.get_label() for l in lines], bbox_to_anchor=(0.71, 0.50), prop={'size':9})
            else:
                plt.ax1.legend(lines, [l.get_label() for l in lines], bbox_to_anchor=(1.20, 1.10), prop={'size':9})
        else:
            if BOOK:
                setFigLinesBW(self.fig5)
                if self.winch.name == "Winch_20000N_4.4_8.8":
                    plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':11})
                else:
                    plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':12})
            else:
                if self.winch.name == "Winch_20000N_4.4_8.8":
                    plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':10})
                else:
                    plt.ax1.legend(lines, [l.get_label() for l in lines], loc='best', prop={'size':11})
        plt.ax1.grid(True, color='0.5')
        self.fig5.tight_layout()
        output_dir = expanduser('~/00AWE_Book/GroundStation/figures')
        createDir(output_dir)
        base_file_name = expanduser('~/00AWE_Book/GroundStation/figures/efficiencies_' + self.winch.name)
        if USEPGF:
            plt.savefig(base_file_name + '.pgf')
            print "Written file: "+ base_file_name + '.pgf'
        plt.savefig(base_file_name + '.png', dpi=300)
        plt.savefig('./results/'+filename+'_efficiencies.png')
        print "Written file: "+ base_file_name + '.png'
        print "Finshed!"
        return self.fig5

    def plotWindSpeed(self, filename):
        """
        Read simulation result from file (.json format) and
        plot the windspeed in 25m height and at the height of the kite
        """
        fileObj = open('./results/'+filename+'.json',"r") # open for for read
        result_str = fileObj.read()
        fileObj.close()
        results = jsonpickle.decode(result_str)
        (listX, list_ws_25m, list_ws_kite_height) = ([], [], [])
        for result in results:
            listX.append(result['v_wind_ground'])
            list_ws_25m.append(self.calcWindHeight(result["v_wind_ground"], 25))
            list_ws_kite_height.append(self.calcWind(result["l_av_opt"], result["v_wind_ground"],
                                                     result["beta_opt_degree"] / 180.0 * pi))
        self.fig6 = plt.figure(6)
        plt.ax1 = plt.subplot(111)
        plt.ax1.set_ylabel('Wind speed [m/s]')
        line1 = plt.plot(listX, list_ws_25m, linewidth = LINEWIDTH, color="blue",
                         label="Wind speed at 25 m height [m/s]")
        line2 = plt.plot(listX, list_ws_kite_height, linewidth = LINEWIDTH, color="red",
                         label="Wind speed at the height of the kite [m/s]")
        plt.ax1.set_xlabel('Wind speed in 10 m height [m/s]')
        lines = line1 + line2
        plt.ax1.legend(lines, [l.get_label() for l in lines], loc='lower right')
        plt.ax1.grid(True, color='0.5')
        self.fig6.tight_layout()
        plt.savefig('./results/'+filename+'_wind_speed.png')
        print "Finshed!"
        self.fig6.show()
        return self.fig6

if __name__ == "__main__"        :
    plotTrajectory()
