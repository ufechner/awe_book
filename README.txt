Kite Power System Optimizer
---------------------------

This software was written for the book chapter "Model-Based Efficiency Analysis
of Wind Power Conversion by a Pumping Kite Power System" of
the book "Airborne Wind Energy", Springer Publishing, 2013.

It can be used to simulate the power output, the reel-in and reel-out speeds,
the forces and efficiencies of kite power systems in pumping mode of operation
(see also: www.kitepower.eu).

This software is published under the LGPL license.

If you have any suggestions and fixes to improve this software please write
an email to: u.fechner@tudelft.nl.

Installation:
-------------

See the file INSTALL.txt

Checking out the software from BitBucket:
-----------------------------------------

cd ~
mkdir 00PythonSoftware
cd 00PythonSoftware
git clone https://bitbucket.org/ufechner/AWE_Book.git

How to use this software:
-------------------------

Main program: Scenarios.py
Execute in current Python or IPython interpreter.
Kites and winch properties defined in Repository.py

The following command sequence displayes the simulation results for
the V2 kite of TU Delft:

cd ~/00PythonSoftware/AWE_Book
ipython
run Scenarios.py
scen.setScenario(1)
scen.plotAll()

Uwe Fechner
Michael Noom

Delft University of Technology, section wind energy, The Netherlands.
