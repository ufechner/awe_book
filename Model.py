# -*- coding: utf-8 -*-
# pylint: disable-msg = W0621
from __future__ import division
"""
* This file is part of SystemOptimizer.
*
* SystemOptimizer -- A kite-power system power optimization software.
* Copyright (C) 2013 by Uwe Fechner, Michael Noom, Delft University
* of technology. All rights reserved.
*
* SystemOptimizer is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* SystemOptimizer is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with SystemOptimizer; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
"""
"""
Module for calculating the optimal operating conditions of a kite power system based on
the averaging framework from Micheal Noom. Defines the class Model and contains some test code
for it.
"""
from Framework import Framework
from openopt import GLP
import numpy as np
#from pylab import figure
from FuncDesigner import oovars, sqrt, sin
from Repository import Repository
from Settings import MAX_ITER, MAX_ITER_INTERALG, USE_SOLVER_DE, OUTER_LOOP

class Model(Framework):
    """
    Class for calculating the optimal operating conditions of a kite power system based on
    the averaging framework from Micheal Noom.
    """
    def __init__(self, phi, delta, kite, winch, d_tether=4.0, max_time = 10):
        Framework.__init__(self, phi, delta, kite, winch)
        self.d_tether = d_tether
        self.plotit   = False
        self.f_tol    = 200  # maximal error wrt the power output [W]
        self.max_time = max_time
        self.reelInEnergy = 0.0
    def calcLoD(self, l_av, paramLoD_kite):
        """
        Calculate the effective lift over drag as a function of the average tether length l_av
        and the lift over drag ration of the kite.
        """
        rel_drag_tether = 0.31
        paramCD_tether = rel_drag_tether * l_av * self.d_tether / (1000 * self.area)
        paramCD = self.paramCL / paramLoD_kite # drag coefficient of the kite
        paramCD_tot = paramCD + paramCD_tether
        return (self.paramCL/ paramCD_tot)

    def calcReeloutSpeed(self, l_av, v_wind_gnd, beta, rel_speed):
        v_wind = self.calcWind(l_av, v_wind_gnd, beta)
        return (v_wind * rel_speed)

    # TODO: remove this function
    def calcForce(self, mass, l_av, v_wind_gnd, relBoC, beta, rel_speed, paramLoD_kite):
        return self.calcPower1(v_wind_gnd, mass, relBoC, beta, rel_speed, l_av, paramLoD_kite)  \
               / self.calcReeloutSpeed(l_av, v_wind_gnd, beta, rel_speed)

    def calcPower1(self, v_wind_gnd, mass, relBoC, beta, rel_speed, l_av, paramLoD_kite):
        """
        Calculate the average mechanical reel-out power based on pulling force one. Must be identical
        with the result of calcPower2. Parameter to vary is b_over_c.

        **Parameters**

        :v_wind_gnd:           average wind speed at the ground (6m height)
        :mass:                 mass of the kite including pod and part of the tether
        :param b_over_c:       b/c (see fig. 3.1 in main.pdf)
        :param beta:           elevation angle in radian
        :param rel_speed:      reel-out speed devided by wind speed
        :param l_av:           average tether length
        :param paramLoD_kite:  effective lift over drag ratio of the kite only
        :result:               average mechanical reel-out power
        """
        paramLoD = self.calcLoD(l_av, paramLoD_kite)
        b_over_c = paramLoD * relBoC
        v_wind = self.calcWind(l_av, v_wind_gnd, beta)
        res = self.calcF1(v_wind, mass, b_over_c, beta, rel_speed, paramLoD)
        return (res * v_wind * rel_speed)

    def calcCyclePower(self, v_wind_gnd, beta, rel_speed, l_av, paramLoD_kite):
        mass = 0
        paramLoD = self.calcLoD(l_av, paramLoD_kite)
        v_wind = self.calcWind(l_av, v_wind_gnd, beta)
        res = self.calcF1(v_wind, mass, paramLoD, beta, rel_speed, paramLoD) / self.winch.crest_factor_force
        reel_out_time = (self.delta_length / (v_wind * rel_speed)) + self.transition_time
        reelOutEnergy = (reel_out_time - self.transition_time) * res * v_wind * rel_speed
        reel_in_time = self.reel_in_time
        reelInEnergy = self.reelInEnergy
        #print "--> ", reelOutEnergy, reelInEnergy, self.reel_out_time, reel_in_time
        netMechPower = (reelOutEnergy + reelInEnergy) / (reel_out_time + reel_in_time)
        return netMechPower


    def calcPowerMax(self, v_wind_gnd, beta, rel_speed, l_av, paramLoD_kite):
        mass = 0
        paramLoD = self.calcLoD(l_av, paramLoD_kite)
        v_wind = self.calcWind(l_av, v_wind_gnd, beta)
        res = self.calcF1(v_wind, mass, paramLoD, beta, rel_speed, paramLoD) / self.winch.crest_factor_force
        return (res * v_wind * rel_speed)


    def optimize1(self, v_wind_gnd, solver='de', log = True, last_beta = None, last_lav = None,
                  last_rel_speed = None):
        """
        Optimize the elevation angle beta for maximal force. The mass is not taken into account.

        **Parameters**

        :v_wind_gnd: average wind speed at the ground (6m height)
        """

        print "optimize1", v_wind_gnd, last_beta, last_lav, last_rel_speed
        mass = 0
        beta1, rel_speed1, l_av1, paramLoD1 = oovars('beta1', 'rel_speed1', 'l_av1', 'paramLoD1')

        if self.winch.force_max > 12000:
            SCALING = sqrt(19.7 / self.area) * (self.winch.force_max / 12634.0)**(1/3.0)
        else:
            SCALING = sqrt(19.7 / self.area)
        V_BASE2 = 9.0 * SCALING
        if v_wind_gnd >= V_BASE2:
            min_LoD = self.paramLoD - (v_wind_gnd - V_BASE2) / 2.5 # was 2.5
        else:
            min_LoD = self.paramLoD

        max_LoD = min_LoD + 1.5
        if max_LoD > self.paramLoD:
            max_LoD = self.paramLoD

        # determine good estimates for the minimal and maximal values of beta
        b_min = 20.5 * (np.pi/180.0)

        V_BASE = 7.0 * SCALING # wind speed, when it is needed to start to increase the elevation angle
        V_INC  = SCALING

        if (last_beta is not None) and (v_wind_gnd >= 10.0):
            b_max = last_beta + 10.0 * (np.pi/180.0)
            b_min = last_beta -  5.0 * (np.pi/180.0)
        else:
            if v_wind_gnd > V_BASE and v_wind_gnd <= V_BASE + V_INC:
                b_max = (44.0 + 2.3 * (v_wind_gnd-7.0))  * (np.pi/180.0)
                b_min = 20.0 * (np.pi/180.0)
            elif v_wind_gnd > V_BASE + V_INC and v_wind_gnd <= V_BASE + 2.0 * V_INC:
                b_max = (48.0 + 2.3 * (v_wind_gnd-7.0))  * (np.pi/180.0)
                b_min = 20.0 * (np.pi/180.0)
            elif v_wind_gnd > V_BASE + 2.0 * V_INC and v_wind_gnd <= V_BASE + 5.0 * V_INC:
                b_max = (54.0 + 2.3 * (v_wind_gnd-7.0))  * (np.pi/180.0)
                b_min = 35.0 * (np.pi/180.0)
            elif v_wind_gnd > V_BASE + 5.0 * V_INC and v_wind_gnd <= V_BASE + 8.0 * V_INC:
                b_max = (56.0 + 2.3 * (v_wind_gnd-7.0))  * (np.pi/180.0)
                b_min = 40.0 * (np.pi/180.0)
            elif v_wind_gnd > V_BASE + 8.0 * V_INC:
                b_max = (56.0 + 2.3 * (v_wind_gnd-7.0))  * (np.pi/180.0)
                b_min = 50.0 * (np.pi/180.0)
            else:
                b_max = 40.0 * (np.pi/180.0)

        if b_max > 67.0 * (np.pi/180.0):
            b_max = 67.0 * (np.pi/180.0)

        LMAX = 600.0 # max average tether length
        LMIN = 200.0 # min average tether length
        DMAX = 100.0
        if last_lav is None:
            l_av_start = (LMAX + LMIN) / 2.0
            l_av_min = LMIN
            l_av_max = LMAX
        else:
            if last_lav > (LMAX-1.0 - DMAX):
                last_lav = LMAX-1.0 - DMAX
            if last_lav < (LMIN + DMAX):
                last_lav = LMIN + DMAX
            l_av_start = last_lav
            l_av_min = last_lav - DMAX
            l_av_max = last_lav + DMAX

#        if last_beta is None:
#            last_beta = (b_min + b_max) / 2.0
#        print "xxx> last_beta, b_min, b_max, l_av_start: ", last_beta, b_min, b_max, l_av_start

        if last_rel_speed is None:
            rel_speed_start = 0.33
        else:
            rel_speed_start = last_rel_speed

        if v_wind_gnd >= 8.0:
            rel_speed_min = 0.11
        else:
            rel_speed_min = 0.15

        startPoint  = {beta1:(b_max+b_min) / 2.0, rel_speed1:rel_speed_start, l_av1: l_av_start,  \
                       paramLoD1:(min_LoD + self.paramLoD) / 2.0}

        if self.reel_in_time == 0:
            power1 =  -self.calcPowerMax(v_wind_gnd, beta1, rel_speed1, l_av1, paramLoD1)
        else:
            power2 =  -self.calcCyclePower(v_wind_gnd, beta1, rel_speed1, l_av1, paramLoD1)

        if log:
            print "b_min, b_max:    " , b_min * 180/np.pi , b_max * 180/np.pi
            print "l_av_min, l_av_max", l_av_min, l_av_max
            print "rel_speed_start, min_LoD, max_LoD", rel_speed_start, min_LoD, max_LoD

        v_reel_out_max_av = self.winch.v_reel_out_max / self.winch.crest_factor_speed
        max_force = self.winch.force_max / self.winch.crest_factor_force
        max_power = self.winch.getMaxAverageMechInputPower()

        constraints = [beta1 >= b_min, beta1 <= b_max, rel_speed1 >= rel_speed_min, rel_speed1 <= 0.7, \
            self.calcPowerMax(v_wind_gnd, beta1, rel_speed1, l_av1, paramLoD1) <= max_power, l_av1 >= l_av_min, \
            self.calcForce(mass, l_av1,v_wind_gnd, 1.0, beta1, rel_speed1,  paramLoD1) <= max_force, l_av1 <= l_av_max, \
            self.calcReeloutSpeed(l_av1, v_wind_gnd, beta1, rel_speed1) <= v_reel_out_max_av, \
            paramLoD1 >= min_LoD, paramLoD1 <= max_LoD
            ]

        if self.reel_in_time == 0:
            problem = GLP(power1, startPoint, fTol = self.f_tol)
            problem.name = 'OptimizeReelOutPower'
        else:
            problem = GLP(power2, startPoint, fTol = self.f_tol)
            problem.name = 'OptimizeCyclePower'
        problem.constraints = constraints
        if min_LoD == max_LoD:
                problem.fixedVars = {paramLoD1: min_LoD}
        if log:
            if solver == 'interalg':
                iprint = 16
            else:
                iprint = 100
        else:
            iprint = -1
        problem.maxIter = MAX_ITER
        if solver == 'de':
            result = problem.solve('de', iprint=iprint, \
                               maxNonSuccess = 5000, searchDirectionStrategy='best', maxFunEvals = 3e5,
                               baseVectorStrategy = 'best', plot=self.plotit, ftol = 1e-2) #seed = 150880,
        else:
            # use solver de to find a good starting point for solver interalg
            result = problem.solve('de', iprint=100, \
                               maxNonSuccess = 5000, searchDirectionStrategy='best', maxFunEvals = 3e5,
                               baseVectorStrategy = 'best', plot=self.plotit, ftol = 1e-2) #seed = 150880,
            beta_opt, rel_speed_opt, l_av_opt, optLoD = result(beta1, rel_speed1, l_av1, paramLoD1)
            if last_beta is None:
               beta_start = beta_opt
            else:
               print "beta_opt, last_beta: ", beta_opt, last_beta
               #beta_start = (beta_opt + last_beta) / 2.0
               beta_start = beta_opt
            startPoint  = {beta1:beta_start, rel_speed1:rel_speed_opt, l_av1: l_av_opt,  \
                       paramLoD1:optLoD}
            if self.reel_in_time == 0:
                problem = GLP(power1, startPoint, fTol = self.f_tol)
                problem.name = 'OptimizeReelOutPower'
            else:
                problem = GLP(power2, startPoint, fTol = self.f_tol)
                problem.name = 'OptimizeCyclePower'
            problem.constraints = constraints
            problem.maxIter = MAX_ITER_INTERALG
            # try to find an improved solution with solver interalg
            # alternative: 'gsubg'; needs a NSP problem definition
            result = problem.solve('interalg', iprint=iprint, \
                               maxNonSuccess = 5000, maxActiveNodes = 2000, #dataHandling="sorted"
                               maxNodes = 2e6, dataHandling = 'raw', plot=self.plotit) #seed = 150880

        beta_opt, rel_speed_opt, l_av_opt, optLoD = result(beta1, rel_speed1, l_av1, paramLoD1)
        cycle_power_opt = self.calcCyclePower(v_wind_gnd, beta_opt, rel_speed_opt, l_av_opt, optLoD)
        ro_power_opt = self.calcPowerMax(v_wind_gnd, beta_opt, rel_speed_opt, l_av_opt, optLoD)
        reel_out_speed = self.calcReeloutSpeed(l_av_opt, v_wind_gnd, beta_opt, rel_speed_opt)
        h_av = l_av_opt * sin(beta_opt)
        v_wind_height = self.calcWindHeight(v_wind_gnd, h_av)
        if log:
            print "beta_opt:       " + str(beta_opt * 180.0 / np.pi)
            print "rel_speed_opt:  " + str(rel_speed_opt)
            print "cycle_power_opt:" + str(cycle_power_opt)
            print "ro_power_opt:   " + str(ro_power_opt)
            print "LoD_kite_opt:   " + str(optLoD)
            print "effective LoD:  " + str(self.calcLoD(l_av_opt, optLoD))
            print "reel-out speed: " + str(reel_out_speed)
            print "av. tether length:  " + str(l_av_opt)
            print "av. tether force:   " + str(self.calcForce(mass, l_av_opt, v_wind_gnd, 1.0, beta_opt, \
                                              rel_speed_opt, optLoD))
            print "h_av:               " + str(h_av)
            print "v_wind_height:      " + str(v_wind_height)

        istop = result.istop
        if not result.isFeasible:
            raise Exception("Solver failed. No feasible solution found!")
        print "===>> Solver istop: ", istop
        if istop < 0 and istop != -9 and istop != -7: # -9: max time limit reached, -7 max iter reached
            raise Exception("Solver failed!")

        return (cycle_power_opt, ro_power_opt, beta_opt, 1.0, l_av_opt, reel_out_speed, rel_speed_opt)

    def optimizeFullCycle(self, v_wind_gnd, delta_length, last_beta = None, last_lav = None,
                          last_rel_speed = None):
        # 1. determine beta_opt, power_opt, l_av_opt
        # 2. determine reel_out_time and reel_out_power
        # 3. repeat 1. and 2. until result converged
        # self.plotit = False
        result = {}
        print "--> v_wind_gnd, self.max_time ", v_wind_gnd, self.max_time
        for i in range(OUTER_LOOP):
            if (i == 0) or (i == 2) or (i == 4) or (i == 6) or (i == 8):
                solver = 'de'
                # last_beta = None
            else:
                if USE_SOLVER_DE:
                    solver = 'de'
                else:
                    solver = 'interalg'
            cycle_power_opt, ro_power_opt, beta_opt, dummy, l_av_opt, reel_out_speed, rel_speed_opt \
                = self.optimize1(v_wind_gnd, solver, True, last_beta, last_lav, last_rel_speed)
            reel_out_time = delta_length / reel_out_speed
            print "Simulate reel-in phase..."
            netMechPower, reelInEnergy, reelOutEnergy = self.reelIn(l_av_opt + delta_length/2.0, beta_opt,
                                                                   reel_out_time, ro_power_opt, delta_length,
                                                                   v_wind_gnd, log = False)

            print "Reel-in finshed: ", netMechPower, reelInEnergy, reelOutEnergy
            if (i==0) and (last_beta is None):
                last_beta = beta_opt
                last_lav = l_av_opt
            if i == 1 or ((i > 1) and (netMechPower > result['netMechPower'])):
                ## calculate electrical efficiency
                self.winch.stored_energy = 0
                reel_in_speed = delta_length / self.reel_in_time
                print "------->> reelOutEnergy, reel_out_time, reel_out_speed: ", reelOutEnergy, reel_out_time, \
                                                                                  reel_out_speed
                eta_ro = self.winch.calcEtaReelOut(reelOutEnergy, reel_out_time, reel_out_speed)
                print "------->> reelInEnergy, reel_in_time, reel_in_speed: ", reelInEnergy, self.reel_in_time, \
                                                                                  reel_in_speed
                eta_ri = self.winch.calcEtaReelIn(abs(reelInEnergy), self.reel_in_time, reel_in_speed)
                print "------->> eta_ro, eta_ri: ", eta_ro, eta_ri
                print "netMechPower, grossElectricPower", netMechPower, self.winch.stored_energy/ \
                    (reel_out_time + self.reel_in_time)

                net_elec_power = self.winch.stored_energy / (reel_out_time + self.reel_in_time)
                print "netMechPower, net_elec_power", netMechPower, net_elec_power
                result = {'v_wind_ground': float(v_wind_gnd), 'l_av_opt': float(l_av_opt),
                          'reel_out_time' : float(reel_out_time), 'reel_in_time' : float(self.reel_in_time),
                          'beta_opt_degree': float(beta_opt/np.pi) * 180.0 ,'netMechPower' : float(netMechPower),
                          'net_elec_power' : float(net_elec_power),
                          'reelInEnergy' : float(reelInEnergy), 'reelOutEnergy': float(reelOutEnergy),
                          'solver' : solver, 'reel_out_speed': float(reel_out_speed),
                          'rel_speed_opt': float(rel_speed_opt)}
                duty_cycle = (100.0 * result['reel_out_time'] \
                                         / (result['reel_out_time'] + result['reel_in_time']))
                print "duty_cycle: ", duty_cycle


            print "i: ", i, solver, netMechPower, result
        return result


if __name__ == "__main__":
    repo = Repository()
    # define kite and winch
    kite = repo.getKite('Kite_165_LoD_5_2')
    winch = repo.getWinch('EMCE_4000N_8_8')
    ## define tether
    d_tether = 4.0 # tether diameter in mm
    ## operational parameters
    phi   = 0 * (np.pi/180)
    delta = 0 * (np.pi/180)
    delta_length = 200 # max tether length - min tether length [m]
    ## create framework object
    model = Model(phi, delta, kite, winch, d_tether)

    ## wind velocity
    v_wind_ground =  10.0

    # simulation parameters
    model.max_time = 240 # max time per optimization [s] was: 240
    model.plot     = True
    model.f_tol    =  100 # max error w.r.t. the power output [W]
    model.transition_time = 5.0
    if True:
        result = model.optimizeFullCycle(v_wind_ground, delta_length)
    if False:
        model.optimize1(v_wind_ground, solver='de')
#        if model.plotit:
#            figure(20)
    if False:
        model.optimize1(v_wind_ground)
